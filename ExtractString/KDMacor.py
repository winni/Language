# -*- coding:utf-8 -*-
'''
Created on Dec 24, 2014

@author: naruto
'''
#需要检索的路径 '/doc/workspace/java/ConvertLanguage/src/test'
KD_ROOT_PATH = '/Users/Green/doc/workspace/taxi4.0'
#不需要检索的文件夹
KD_CONVERT_NO_DOC = ',Resources,CoreData,'
#检索的文件类型
KD_CONVERT_FILE_TYPE = ',.h,.m,.mm,.c,'
#过滤的文件
KD_CONVERT_NO_FILE = ',Localizable.strings,'
#文件路径（不需要检索的字符串）
KD_CONVERT_NO_STRING_FILE = '/Users/Green/doc/workspace/taxi4.0/KDTaxicab/en.lproj/Localizable.strings'
if __name__ == '__main__':
    print('请运行 main.py')