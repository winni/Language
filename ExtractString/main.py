'''
Created on Jan 6, 2015

@author: naruto
'''

from curses.ascii import NUL
import KDMacor
import ConvertLang


if __name__ == '__main__':
    objConv=ConvertLang.ConvertLang()
    print('开始加载字符串黑名单...')
    objConv.loadBlackList(KDMacor.KD_CONVERT_NO_STRING_FILE)
    print('开始检索目录...')
    objConv.searchDocument(KDMacor.KD_ROOT_PATH)
    print('开始保存简体字符串...')
    objConv.saveLocaleData(objConv.listLocale,'Hans')
    print('开始保存繁体字符串...')
    objConv.saveHantLocaleData(objConv.listLocale,'Hant')
    del objConv